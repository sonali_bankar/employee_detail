import Title from "../components/Title"
import Image from "next/image";
export default function Contact() {
    return (
      <>
      <Title title="Contact"/>
      <div className ="flex m-11 p-5 shadow-md ">
      <Image src="/comapy.jpeg" width={500} height={300}  className=""/>
      <div  className="w-full  ">
     <form  className="bg-white rounded px-8 pt-6 pb-8 mb-4">
    <div  className="mb-4">
      <label  className="block text-gray-700 text-sm font-bold mb-2" >
       Name
      </label>
      <input  className="shadow appearance-none border rounded w-full
       py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
       id="username" type="text" placeholder="Username"/>
    </div>
    <div  className="mb-6">
      <label  className="block text-gray-700 text-sm font-bold mb-2" >
       Email
      </label>
      <input  className="shadow appearance-none border 
      rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none 
      focus:shadow-outline" type="email" placeholder="Email"/>
  
    </div>
    <div  className="mb-6">
      <label  className="block text-gray-700 text-sm font-bold mb-2" >
       Message
      </label>
      <textarea className="shadow appearance-none border 
      rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none 
      focus:shadow-outline" type="text" placeholder="Wrire here..."/>
  
    </div>
    <div className="flex items-center justify-between">
      <button className="bg-blue-500 hover:bg-blue-700 
      text-white font-bold py-2 px-4 rounded focus:outline-none f
      ocus:shadow-outline" type="button">
       Submit
      </button>
    
    </div>
  </form>
  </div>
  </div>
      </>
    )
  }
  