import Link from 'next/link'
export default function Navbar() {
    return (
        <header className="bg-gray-800 text-white p-5 ">
      
        <nav className="mb-5 mt-auto text-md  uppercase font-800 pl-5">
            <Link href="/" className="p-6 pb-3 hover:underline  "><a className="p-5"> Home</a></Link>
            <Link href="/about" className="p-6 hover:underline"><a className="p-5">About</a></Link>
            <Link href="/contact" className="p-6 hover:underline"><a className="p-5">Contact</a></Link>
        </nav>
    </header>

    )
  }