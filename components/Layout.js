import Navbar from "./Navbar";
import Hero from "./Hero"
import Footer from "./Footer";

export default function Layout(props) {
    return (
        <>       
      <Navbar/>  
      <Hero />    
    <main>
        {props.children}
    </main>  
    <Footer/> 
    </>
    )
  }
  