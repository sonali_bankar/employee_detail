import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook } from "@fortawesome/free-brands-svg-icons"
import { faTwitter } from "@fortawesome/free-brands-svg-icons"
import styles from '../styles/Home.module.css'

export default function Footer() {
    return (
        <>       
     <footer className="bg-gray-800 p-10 "> 
     <div className="flex justify-end text-white" >
     <FontAwesomeIcon icon={faFacebook} className={styles.social}/>
     <FontAwesomeIcon icon={faTwitter} className={styles.social}/>
     </div>
    <h2  className="text-center m-auto text-white font-300 font-serif p-5">Employee Information Website &copy; all right reserved.</h2>

     </footer>
    </>
    )
  }
  